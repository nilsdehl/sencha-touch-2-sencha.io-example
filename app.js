Ext.Loader.setPath({
	'Ext.io': 'libs/sencha-io/src/io',
	'Ext.cf': 'libs/sencha-io/src/cf',
	'Ext': 'sdk/src',
	'Ux' : 'libs/Ux',
	'PathMenu': 'modules/PathMenu',
	'Share': 'modules/Share',
	'Photos': 'modules/Photos'
});
Ext.application({
	name: 'IoExample',

	controllers: [
		//include Ext.io.Controller to manage the sencha.io connection
		'Ext.io.Controller',
		// Controller for the main application
		'Application',
		// Controller of the module to manage photos
		'Photos.controller.Photos',
		// Controller of the module for sharing images between users
		'Share.controller.Share',
		// Controller for the path menu
		'PathMenu.controller.Menu'
	],

	/**
	 * Add sencha.io app configuration.
	 */
	config: {
		io: {
			// app id string configured on http://developer.sencha.io/apps
			appId: 'BWZo0fc1h0IB68mupdaLDf7YoS1',
			// app secret
			appSecret: 'BbTiHTZHnQGPJgUc',
			// logging level. Should be one of "none", "debug", "info", "warn" or "error". Defaults to "error".
			logLevel: 'error',
			// If you don't want to attempt to authenticate on startup set this to false. defaults to true
			authOnStartup: true,
			// If you don't want to automatically trigger the login panel when your application starts then set manualLogin to true
			manualLogin: false
		}
	},

	requires: ['Ext.MessageBox'],

	viewport: {
		autoMaximize: true,
		showAnimation: 'slideIn'
	},

	// app icons
	icon: {
		57: 'resources/icons/Icon.png',
		72: 'resources/icons/Icon~ipad.png',
		114: 'resources/icons/Icon@2x.png',
		144: 'resources/icons/Icon~ipad@2x.png'
	},

	// start screens
	phoneStartupScreen: 'resources/loading/Homescreen.jpg',
	tabletStartupScreen: 'resources/loading/Homescreen~ipad.jpg',


	/**
	 * Application launch
	 */
	launch: function() {
		// Destroy the #appLoadingIndicator element
		Ext.fly('appLoadingIndicator').destroy();
	},

	/**
	 * On production build app updated
	 */
	onUpdated: function() {
		Ext.Msg.confirm(
			"Application Update",
			"This application has just successfully been updated to the latest version. Reload now?",
			function(buttonId) {
				if (buttonId === 'yes') {
					window.location.reload();
				}
			}
		);
	}
});
