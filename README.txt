This is an example SenchaTouch 2 app which uses the Sencha.io cloud services.

It uses the services login, data, message and src to manage and share images.

Article: http://www.sencha.com/blog/sencha-io-photo-sharing-example-using-sencha-touch
Video: https://vimeo.com/44054152

Sencha.io -> http://sencha.io
Sencha.io Doku -> http://docs.sencha.io/


Have Fun!

Nils Dehl
@nilsdehl
http://nils-dehl.de