Ext.define('IoExample.view.Home', {
	extend: 'Ext.Container',
	alias: 'widget.home',
	requires: ['Ext.Img'],

	config: {
		scrollable: true,

		layout: 'vbox',
		cls: 'bg',
		items: [
			{
				xtype: 'img',
				src: 'resources/images/sencha-io.png',
				style: 'background-position: center;',
				width: 320,
				height: 179
			},
			{
				cls: 'home',
				html: 'Sencha.io cloud showcase app.<br> ' +
					'Find the code in git repo on<br><b>bitbucket.org/nilsdehl</b>' +
					'<br /><br />Example by Nils Dehl <br><b>@nilsdehl</b>',
				styleHtmlContent: true
			}
		]

	}
});